import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddComponent } from './add/add.component';
import { AllComponent } from './all/all.component';
import { DeleteComponent } from './delete/delete.component';
import { HomeComponent } from './home/home.component';
import { ResultComponent } from './result/result.component';
import { SinglePlantComponent } from './single-plant/single-plant.component';
import { UpdatingComponent } from './updating/updating.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'all', component: AllComponent },
  { path: 'add', component: AddComponent },
  { path: 'add/all', redirectTo: 'all', pathMatch: 'full' },
  { path: 'result/:type', component: ResultComponent },
  { path: 'single-plant/:id', component: SinglePlantComponent },
  { path: 'result/single-plant/:id', redirectTo: 'single-plant/:id', pathMatch: 'full' },
  { path: 'update/:id', component: UpdatingComponent },
  { path: 'delete/:id', component: DeleteComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
