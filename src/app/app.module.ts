import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms'
import { ReactiveFormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AllComponent } from './all/all.component';
import { AddComponent } from './add/add.component';
import { SearchComponent } from './search/search.component';
import { ResultComponent } from './result/result.component';
import { SinglePlantComponent } from './single-plant/single-plant.component';
import { UpdatingComponent } from './updating/updating.component';
import { DeleteComponent } from './delete/delete.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AllComponent,
    AddComponent,
    SearchComponent,
    ResultComponent,
    SinglePlantComponent,
    UpdatingComponent,
    DeleteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
