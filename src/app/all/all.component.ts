import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Plant } from '../interface/plant.interface';
import { PlantBackService } from '../service/plant-back.service';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.scss']
})
export class AllComponent implements OnInit {

  plants: Plant[] = []
  plant: Plant = {} as Plant

  constructor(
    private service: PlantBackService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    this.service.getAll().subscribe(allPlants => {
      this.plants = allPlants;
    })
  }

}
