import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Plant } from '../interface/plant.interface';
import { PlantBackService } from '../service/plant-back.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {

  plants: Plant[] = []

  constructor(
    private service: PlantBackService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    const search = String(this.route.snapshot.paramMap.get('search'));
    this.searchAny(search);
  }

  searchAny(search: string) {
    this.service.searchAny(search).subscribe(result => {
      this.plants = result;
    })
  }
}
