export interface Plant {
    id?: number;
    name: string;
    type: string;
}