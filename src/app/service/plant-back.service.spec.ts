import { TestBed } from '@angular/core/testing';

import { PlantaBackService } from './plant-back.service';

describe('PlantaBackService', () => {
  let service: PlantaBackService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlantaBackService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
