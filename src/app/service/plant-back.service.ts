import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Plant } from '../interface/plant.interface'
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class PlantBackService {

  constructor(
    private http: HttpClient
  ) { }

  getAll() : Observable<Plant[]> {
    return this.http.get<Plant[]>("http://localhost:8080/plant")
  }

  findById(id: number) : Observable<Plant> {
    return this.http.get<Plant>("http://localhost:8080/plant/" + id)
  } 

  findByName(name: string) : Observable<Plant> {
    return this.http.get<Plant>("http://localhost:8080/plant/name/" + name)
  }

  findByType(type: string) : Observable<Plant[]> {
    return this.http.get<Plant[]>("http://localhost:8080/plant/type/" + type)
  }

  searchAny(search: string) : Observable<Plant[]> {
    return this.http.get<Plant[]>("http://localhost:8080/plant/search/" + search)
  }

  addPlant(plant: Plant) : Observable<Plant> {
    return this.http.post<Plant>("http://localhost:8080/plant", plant)
  }

  updatePlant(plant: Plant) : Observable<Plant> {
    return this.http.put<Plant>("http://localhost:8080/plant/update", plant)
  }

  deletePlant(id: number) : Observable<Plant> {
    return this.http.delete<Plant>("http://localhost:8080/plant/" + id)
  }
   
}
