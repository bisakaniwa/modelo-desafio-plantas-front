import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Plant } from '../interface/plant.interface';
import { PlantBackService } from '../service/plant-back.service';
import { Validators } from '@angular/forms'
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  plant: Plant = {} as Plant

  constructor(
    private service: PlantBackService,
    private form: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  addForm = this.form.group({
    name: ['', Validators.required],
    type: ['', Validators.required],
  });

  adding(plant: Plant) {
    if (this.addForm.value.name !== '' && this.addForm.value.type !== '') {
      this.plant.name = this.addForm.value.name!;
      this.plant.type = this.addForm.value.type!;

      this.service.addPlant(plant).subscribe(newPlant => {
        this.plant = newPlant;
        this.router.navigate(['/all']);
      })
    }
    alert("There you go!")
  }
}
