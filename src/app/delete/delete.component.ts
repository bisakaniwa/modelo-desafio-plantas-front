import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Plant } from '../interface/plant.interface';
import { PlantBackService } from '../service/plant-back.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {

  plant: Plant = {} as Plant

  constructor(
    private service: PlantBackService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.findPlant(id);
  }

  findPlant(id: number) {
    this.service.findById(id).subscribe(result => {
      this.plant = result;
    })
  }

  deletePlant(id: number) {
    this.service.deletePlant(id).subscribe(deleting => {
      this.plant = deleting;
    })
    alert("It's gone...")
    this.router.navigate(['/all']);
  }

}
