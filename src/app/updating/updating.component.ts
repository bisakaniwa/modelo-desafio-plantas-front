import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Plant } from '../interface/plant.interface';
import { PlantBackService } from '../service/plant-back.service';

@Component({
  selector: 'app-updating',
  templateUrl: './updating.component.html',
  styleUrls: ['./updating.component.scss']
})
export class UpdatingComponent implements OnInit {

  plant: Plant = {} as Plant

  constructor(
    private service: PlantBackService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.findPlant(id);
  }

  findPlant(id: number) {
    this.service.findById(id).subscribe(result => {
      this.plant = result
    });
  }

  updateData(plant: Plant) {
    this.service.updatePlant(plant).subscribe(updated => {
      this.plant = updated
    })
    alert("There you go!")
    this.router.navigate(['/single-plant/', plant.id]);
  }

}
