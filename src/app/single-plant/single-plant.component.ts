import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Plant } from '../interface/plant.interface';
import { PlantBackService } from '../service/plant-back.service';

@Component({
  selector: 'app-single-plant',
  templateUrl: './single-plant.component.html',
  styleUrls: ['./single-plant.component.scss']
})
export class SinglePlantComponent implements OnInit {

  plant: Plant = {} as Plant

  constructor(
    private service: PlantBackService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.getPlant(id);
  }

  getPlant(id: number) {
    this.service.findById(id).subscribe(result => {
      this.plant = result
    })
  }

  updateData(id: number) {
    this.router.navigate(['/update', id])
  }

  deletePlant(id: number) {
    this.router.navigate(['/delete', id])
  }
}
